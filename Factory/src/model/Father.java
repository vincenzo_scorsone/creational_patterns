package model;

public abstract class Father {
	private String name;
	
	public Father(){
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
}
