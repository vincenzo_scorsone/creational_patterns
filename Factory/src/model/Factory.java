/* Il pattern Factory viene utilizzato quando abbiamo una classe padre e delle classi figlie e vogliamo 
 * ottenere delle istanze delle sottoclassi da input, senza doversi preoccupare di istanziare manualmente
 * la classe. In poche parole il pattern toglie il compito di istanziare oggetti al client delegandolo alla
 * Factory. La Factory si può implementare o con il Singleton o con il metodo get statico. L'oggetto 
 * ritornato dipende dal parametro in input. 
 * 
 * VANTAGGI:
 * -Fornisce un approccio al codice per interfaccia piuttosto che per implementazione
 * -Rende il codice più robusto, disaccopiato e facile da estendere. Infatti si potrebbe cambiare la classe
 *  Father lasciando il client ignaro di ciò
 * -Fornisce l'astrazione dall'implementazione al client attraverso l'ereditarietà */
package model;

public class Factory {
	public static Father get(String type){
		if(type.compareTo("Son1")==0)
			return new Son1();
		else if(type.compareTo("Son2")==0)
			return new Son2();
		return null;
	}
}
