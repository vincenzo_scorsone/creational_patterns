package test;

import model.Factory;
import model.Father;

public class Test {

	public static void main(String[] args) {
		Father instance1 = Factory.get("Son1");
		Father instance2 = Factory.get("Son2");
		
		System.out.println(instance1+"\n"+instance2);

	}

}
