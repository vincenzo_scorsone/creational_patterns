package model;

public class Son2 extends Father {
	public Son2(){
		this.setName("Son2");
	}

	@Override
	public String toString() {
		return "I'm " + this.getName();
	}	
}
