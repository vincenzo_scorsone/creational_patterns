package model;

public class Son1Factory implements AbstractFactory{

	@Override
	public Father get() {
		return new Son1();
	}

}
