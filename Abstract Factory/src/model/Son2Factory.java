package model;

public class Son2Factory implements AbstractFactory{

	@Override
	public Father get() {
		return new Son2();
	}

}
