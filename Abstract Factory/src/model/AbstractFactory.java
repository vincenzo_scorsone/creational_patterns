/* A differenza del Factory, che riceve un input e ritorna l'istanza riferita a quell'input, con l'Abstract
 * viene eliminato  il blocco if-else e viene creata una Factory per ogni sotto classe. Invece di un valore
 * alla Factory viene passata  un'istanza di una Factory delle classi figlie. 
 * 
 * VANTAGGI:
 * -Fornisce un approccio al codice per interfaccia piuttosto che per implementazione
 * -E' facilmente estensibile aggiungendo una nuova classe figlia e la relativa Factory
 * -E' robusto ed evita la logica condizionale del Factory
 * */
package model;

public interface AbstractFactory {
	public Father get();
}
