package model;

public class Factory {
	public static Father get(AbstractFactory fact){
		return fact.get();
	}
}
