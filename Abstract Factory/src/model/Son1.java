package model;

public class Son1 extends Father {
	public Son1(){
		this.setName("Son1");
	}

	@Override
	public String toString() {
		return "I'm " + this.getName();
	}	
	
}
