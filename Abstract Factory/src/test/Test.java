package test;

import model.Factory;
import model.Father;
import model.Son1Factory;
import model.Son2Factory;

public class Test {

	public static void main(String[] args) {
		Father instance1 = Factory.get(new Son1Factory());
		Father instance2 = Factory.get(new Son2Factory());
		System.out.println(instance1+"\n"+instance2);
	}

}
