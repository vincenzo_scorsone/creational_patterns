/* IL Prototype è utilizzato quando la creazione degli oggetti richiede tempo e risorse e si hanno oggetti simili già esistenti.
 * Prototype fornisce un meccanismo per copiare un oggetto esistente e poi modificarlo. Se per esempio abbiamo un oggetto che carica
 * dati da un database. Se dobbiamo modificare questi dati più volte, non è una buona idea creare oggetti utilizzando il new e caricando 
 * ogni volta i dati. La migliore soluzione e clonare l'oggetto esistente e modificarlo. Prototype è organizzato in modo che sia la stessa
 * classe che si vuole copiare a fornire la funzione di clonazione. 
 * 
 * */
package model;

import java.util.ArrayList;
import java.util.List;

public class InstancePrototype implements Cloneable {
	private List<Instance> instanceList;
	
	public InstancePrototype(){
		instanceList= new ArrayList<Instance>();
	}
	
	public InstancePrototype(List<Instance> list){
		instanceList=list;
	}
	
	public void loadData(){
		instanceList.add(new Instance("Instance 1"));
		instanceList.add(new Instance("Instance 2"));
		instanceList.add(new Instance("Instance 3"));
	}

	public List<Instance> getInstanceList() {
		return instanceList;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		List<Instance> temp= new ArrayList<Instance>();
		for (Instance i: this.getInstanceList())
			temp.add(i);
		return new InstancePrototype(temp);
	}
	
	
	
}
