package model;

public class Instance {
	private String data;

	public Instance(){
		
	}
	
	public Instance(String data) {
		this.data = data;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Instance [data=" + data + "]";
	}

	
	
}
