package test;

import java.util.List;

import model.Instance;
import model.InstancePrototype;

public class Test {
	public static void main(String args[]) throws CloneNotSupportedException{
		InstancePrototype prt=new InstancePrototype();
		prt.loadData();
		InstancePrototype prt1= (InstancePrototype) prt.clone();
		List<Instance> list= prt.getInstanceList();
		list.add(new Instance("Instance 4"));
		List<Instance> list1= prt1.getInstanceList();
		list1.add(new Instance("Instance 5"));
		System.out.println(prt.getInstanceList());
		System.out.println(list);
		System.out.println(list1);

	}
	
}
