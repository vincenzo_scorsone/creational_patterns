/* Il pattern Singleton impedisce di istanziare un oggetto della classe 
 * e garantisce l'esistenza di una sola istanza nella JVM. La classe deve
 * fornire un accesso globale per ottenere l'istanza della classe. 
 * Per implementarlo ci sono approcci differenti ma della regole comuni:
 * -Costruttore privato
 * -Variabile privata statica (è l'unica istanza della classe stessa)
 * -Un metodo statico pubblico che ritorna l'istanza della classe
 * 
 * 	CON LA REFLECTION SI DISTRUGGE IL PATTERN SINGLETON!
 * */

package model;

public class Instance {
	
	private static Instance instance;

	// Istanza usata per il GET EAGER INIZIALIZATION
	//private static Instance instance=new Instance();

	
	
	// fondamentale ridefinire il costruttore di default
	// private, per non permettere di istanziare l'oggetto
	// all'utente
	private Instance() {
	}

	
	
	
	// vari metodi di implementazione del get per fornire 
	//l'unica istanza dell'oggetto, ovviamente in un'implementazione 
	//reale se ne sceglie solo uno

	// GET EAGER INIZIALIZATION
	// L'istanza è creata al tempo di caricamento della classe,
	// lo svantaggio sta nel fatto che l'istanza viene creata
	// anche se non viene mai utilizzata.
	public static Instance getEagerInizialization() {
		return instance;
	}

	// BLOCCO STATIC DI INIZIALIZZAZIONE
	// Il concetto è come quello del metodo EAGER INIZIALIZATION,
	// anche in questo caso l'istanza viene creata a tempo di caricamento
	// della classe 
	/*static {
		try {
			instance = new Instance();
		} catch (Exception e) {
			throw new RuntimeException("Exception occured in"
					+ " creating singleton instance");
		}
	}*/
	
	// LAZY INIZIALIZATION
	// L'istanza viene creata nel punto di accesso globale,
	// può portare problemi in sistemi multithread, per esempio:
	// se non esiste ancora l'istanza e due thread accedono insieme 
	// alla classe, otterranno due diverse istanze e annullerrano 
	// il concetto di singleton
	public static Instance getLazyInizialization(){
		if(instance==null)
			instance=new Instance();
		
		return instance;
	}
	
	// THREAD SAFE 1
	// con i metodi synchronized è possibile gestire la concorrenza, 
	// in questo modo un solo thread alla volta  potrà accedere alla classe.
	// funziona bene ma i metodi synchronized sono meno performanti,
	// dopo che l'istanza è stata creata questo accesso "sicuro" non è 
	// più necessario
	public static synchronized Instance getLazyInizializationThreadSafe1(){
		if(instance==null)
			instance=new Instance();
		
		return instance;
	}
	
	// THREAD SAFE 2
	// questo metodo evita l'inconveniente del metodo precedente, 
	// applicando la sincronizzazione all'interno del blocco if piuttosto
	// che all'intero metodo
	public static Instance getLazyInizializationThreadSafe2(){
		if(instance==null){
			synchronized(Instance.class){
				if(instance==null)
					instance=new Instance();
			}
		}
		
		return instance;
	}
	
	// BILL PUGH
	// viene create un'inner class privata. Quando la classe principale 
	// viene caricata, lo stesso non avviene per la classe inner. Questa 
	// viene caricata quando viene chiamato  il metodo get. Questo
	// metodo è molto utilizzato in quanto non richiede la sincronizzazione
	private static class BillPughSingleton{
		private static final Instance billPughInstance= new Instance();
	}
	
	public static Instance getBillPughInstance(){
		return BillPughSingleton.billPughInstance;
	}
	
	
	
	
	
	
	

}
