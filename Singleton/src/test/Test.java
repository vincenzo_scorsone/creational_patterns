package test;

import model.Instance;

public class Test {
	public static void main(String args[]){
		/* TEST EAGER INZIALIZATION */
		// Tramite l'hash code è possibile vedere che funziona per l'EAGER
		// Per testare è necessario decommentare il blocco static dalla 
		// classe Instance oppure commentare il primo attributo della classe
		// e decommentare il secondo
		/*Instance ist1 = Instance.getEagerInizialization();
		Instance ist2 = Instance.getEagerInizialization();
		System.out.println(ist1+"\n"+ist2);*/
		
		
		/* TEST LAZY INIZIALIZATION */
		/*Instance ist1 = Instance.getLazyInizialization();
		Instance ist2 = Instance.getLazyInizialization();
		System.out.println(ist1+"\n"+ist2);*/
		
		/* TEST THREAD SAFE  */
		// Per testare entrambe le sincronizzazione decommenta l'istanza che vuoi utilizzare 
		// nella classe SingletonThread e commenta l'altra
		/*for (int i=0;  i<20; ++i)
			(new SingletonThread()).start();*/
		
		/* TEST BILL PUGH */
		Instance ist1 = Instance.getBillPughInstance();
		Instance ist2 = Instance.getBillPughInstance();
		System.out.println(ist1+"\n"+ist2);
		
		
	}
}
