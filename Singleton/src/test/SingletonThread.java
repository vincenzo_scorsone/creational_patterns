// Classe necessaria per testare la sincronizzazione
package test;

import model.Instance;

public class SingletonThread extends Thread {

	@Override
	public void run() {
		//Instance ist = Instance.getLazyInizializationThreadSafe1();
		Instance ist = Instance.getLazyInizializationThreadSafe2();
		System.out.println(ist);
	}
	
}
