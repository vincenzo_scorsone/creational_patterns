/* Il Builder è stato introdotto per risolvere dei possibili problemi con Factory e Abstract Factory quando l'oggetto contiene molti 
 * attributi. In questo caso i problemi degli altri due patterns sono:
 * - Troppi argomenti che il client deve passare alla Factory
 * - Alcuni attributi opzionali con la Factory vengono passati forzatamente come NULL
 * - La complessità dell'oggetto si ripercuote sulla Factory
 * 
 * Per risolvere questi problemi si può fare un costruttore con i parametri obbligatori e dei set per i parametri opzionali. Il problema
 * di questo approccio è il fatto che lo stato dell'oggetto rimane inconsistente finchè gli attributi non vengono settati esplicitamente.
 * Il Builder risolve questo problema fornendo un modo di costruire l'oggetto a step. Per implementarlo in java bisogna:
 * - Prima di  tutto bisogna creare una classe statica nidificata (la classe Builder ) e copiare tutti gli argomenti 
 * dalla classe esterna alla classe Builder. E' convezione chiamare la Builder come la classe esterna aggiungendo la parola Builder
 * - La Builder ha un costruttore pubblico con gli attributi richiesti come parametri. 
 * - La Builder ha dei metodi per settare i parametri opzionali che ritornano l'oggetto stesso
 * - L'ultima cosa è quella di fornire un metodo build() che ritorna l'oggetto al client. Per far ciò il costruttore della class che 
 * contiene la Builder deve essere privato.
 * 
 * 
 * */
package model;

public class Instance {
	private String data1;
	private int data2;
	
	//Opzionale
	private boolean data3;
	
	public String getData1() {
		return data1;
	}
	public int getData2() {
		return data2;
	}
	public boolean isData3() {
		return data3;
	}
	
	private Instance(InstanceBuilder builder) {
		super();
		this.data1 = builder.data1;
		this.data2 = builder.data2;
		this.data3 = builder.data3;
	}
	
	
	
	@Override
	public String toString() {
		return "Instance [data1=" + data1 + ", data2=" + data2 + ", data3=" + data3 + "]";
	}



	//Classe Builder
	public static class InstanceBuilder{
		private String data1;
		private int data2;
		private boolean data3;
		
		public InstanceBuilder(String data1, int data2) {
			this.data1 = data1;
			this.data2 = data2;
		}

		public InstanceBuilder setData3(boolean data3) {
			this.data3 = data3;
			return this;
		}
		
		public Instance build(){
			return new Instance(this);
		}
		
		
	}
	
}
